require 'rspec/autorun'
require 'nokogiri'   
require 'open-uri'  

page = Nokogiri::HTML(open('https://xkcd.com/2137/'))

title = 'Title: "' +page.title+'"'

img = 'Image: "http:'+page.css('img')[2].attr('src')+'"'

describe "Parse" do
    
    context 'when img is found' do
        it { expect(img).to eq('Image: "http://imgs.xkcd.com/comics/text_entry.png"') }
    end

    context 'when text is found' do
        it { expect(title).to eq('Title: "xkcd: Text Entry"') }
    end
    
end